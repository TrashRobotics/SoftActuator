#include <Adafruit_PWMServoDriver.h>

#define STICK_X_PIN   A0
#define STICK_Y_PIN   A1
#define STICK_BUTTON_PIN 3

#define PCA9685_ADRESS  0x40  // адреса I2C устройств 

#define SERVO_FREQ 50

// номера пинов и граничные значения поворота сервопривода в мкс
#define A_LINK_PCA_PIN  0
#define B_LINK_PCA_PIN  1
#define C_LINK_PCA_PIN  2

#define A_LINK_MIN_ANGLE  1000
#define A_LINK_MAX_ANGLE  2000

#define B_LINK_MIN_ANGLE  1000
#define B_LINK_MAX_ANGLE  2000

#define C_LINK_MIN_ANGLE  1000
#define C_LINK_MAX_ANGLE  2000

Adafruit_PWMServoDriver pwmsd = Adafruit_PWMServoDriver(PCA9685_ADRESS);

float stickXPos = 0.0;
float stickYPos = 0.0;
bool stickButtonPos = false;

float linkAPos = 0.0;
float linkBPos = 0.0;
float linkCPos = 0.0;


void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(STICK_BUTTON_PIN, INPUT_PULLUP);
  Wire.begin();
  pwmsd.begin();
  pwmsd.setOscillatorFrequency(27000000);
  pwmsd.setPWMFreq(SERVO_FREQ); 
  pwmsd.writeMicroseconds(A_LINK_PCA_PIN, 1500);
  delay(200);
  pwmsd.writeMicroseconds(B_LINK_PCA_PIN, 1500);
  delay(200);
  pwmsd.writeMicroseconds(C_LINK_PCA_PIN, 1500);
  delay(200);
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW);
  stickXPos = -(2*(float)analogRead(STICK_X_PIN)/1024.f - 1.f);
  stickYPos = -(2*(float)analogRead(STICK_Y_PIN)/1024.f - 1.f);
  stickButtonPos = false; //!digitalRead(STICK_BUTTON_PIN);
  
  stickXYTopulseABC(stickXPos, stickYPos);
  
  if((linkAPos > 1800) || (linkBPos > 1800) || (linkCPos > 1800))  digitalWrite(LED_BUILTIN, HIGH);

  setPos(linkAPos, linkBPos, linkCPos);
  delay(100);
}

void stickXYTopulseABC(float posX, float posY){
  linkAPos = posX;
  linkBPos = -posX/2 + 0.866*posY;
  linkCPos = -posX/2 - 0.866*posY;
  
  // Новая версия привода не умеет сжиматься (только растягивается). Поэтому отрицательные значения компонент пропадают. Чтоб их скомпенсировать
  // нужно "поддать" дополнительного растяжения на остальные два привода. Величина компенсации определяется проекцией пропадающего вектора. 
  if(linkAPos <= 0) {
    linkBPos += abs(linkAPos*0.707);
    linkCPos += abs(linkAPos*0.707);
    linkAPos = 0;
  }
  if(linkBPos <= 0) {
    linkAPos += abs(linkBPos*0.707);
    linkCPos += abs(linkBPos*0.707);
    linkBPos = 0;
  }
  if(linkCPos <= 0) {
    linkAPos += abs(linkCPos*0.707);
    linkBPos += abs(linkCPos*0.707);
    linkCPos = 0;
  }

  linkAPos = map(linkAPos*100, 0, 100, A_LINK_MIN_ANGLE, A_LINK_MAX_ANGLE);
  linkBPos = map(linkBPos*100, 0, 100, B_LINK_MIN_ANGLE, B_LINK_MAX_ANGLE);
  linkCPos = map(linkCPos*100, 0, 100, C_LINK_MIN_ANGLE, C_LINK_MAX_ANGLE);

  linkAPos = constrain(linkAPos, A_LINK_MIN_ANGLE, A_LINK_MAX_ANGLE);
  linkBPos = constrain(linkBPos, B_LINK_MIN_ANGLE, B_LINK_MAX_ANGLE);
  linkCPos = constrain(linkCPos, C_LINK_MIN_ANGLE, C_LINK_MAX_ANGLE);
}

void setPos(float posA, float posB, float posC){
  pwmsd.writeMicroseconds(A_LINK_PCA_PIN, posA);
  pwmsd.writeMicroseconds(B_LINK_PCA_PIN, posB);
  pwmsd.writeMicroseconds(C_LINK_PCA_PIN, posC);
}
