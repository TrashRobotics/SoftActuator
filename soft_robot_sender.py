import time
import threading
import socket
import json
import math

host = ("192.168.43.125", 5000)
sendFreq = 30       # слать 20 пакетов в секунду

msg = {"pulses": [900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900]}

LEG0_MIN_PULSE = 900
LEG0_MAX_PULSE = 1700

LEG1_MIN_PULSE = 900
LEG1_MAX_PULSE = 1800

LEG2_MIN_PULSE = 900
LEG2_MAX_PULSE = 1700

LEG3_MIN_PULSE = 900
LEG3_MAX_PULSE = 1800

t = 0       # условная временная шкала для синхронизации кривых движения щупалец
w = 2.5     # "скорость"
phi_0 = 0  #math.pi     # сдвиги фаз для каждой лапы
phi_1 = math.pi
phi_2 = math.pi
phi_3 = 0  #math.pi


sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(json.dumps(msg, ensure_ascii=False).encode("utf8"), host)  # отправляем сообщение в виде json файла
time.sleep(2)


def control():
    global w, t, phi_0, phi_1, phi_2, phi_3

    leg0X, leg0Y = math.sin(w * t + phi_0), math.cos(w * t + phi_0)
    leg1X, leg1Y = math.sin(w * t + phi_1), math.cos(w * t + phi_1)
    leg2X, leg2Y = -math.sin(w * t + phi_2), math.cos(w * t + phi_2)
    leg3X, leg3Y = -math.sin(w * t + phi_3), math.cos(w * t + phi_3)

    leg0ABC = xyToPulseABC(leg0X, leg0Y, LEG0_MIN_PULSE, LEG0_MAX_PULSE)
    leg1ABC = xyToPulseABC(leg1X, leg1Y, LEG1_MIN_PULSE, LEG1_MAX_PULSE)
    leg2ABC = xyToPulseABC(leg2X, leg2Y, LEG2_MIN_PULSE, LEG2_MAX_PULSE)
    leg3ABC = xyToPulseABC(leg3X, leg3Y, LEG3_MIN_PULSE, LEG3_MAX_PULSE)

    msg["pulses"] = [*leg0ABC, *leg1ABC, *leg2ABC, *leg3ABC]


def xyToPulseABC(posX, posY, minPulse, maxPulse):
    linkAPos = posX
    linkBPos = -posX / 2 + 0.866 * posY
    linkCPos = -posX / 2 - 0.866 * posY
    """ 
    Новая версия привода не умеет сжиматься (только растягивается). Поэтому отрицательные значения компонент пропадают. 
    Чтоб их скомпенсировать нужно "поддать" дополнительного растяжения на остальные два привода. 
    Величина компенсации определяется проекцией пропадающего вектора. 
    """
    if linkAPos <= 0:
        linkBPos += abs(linkAPos * 0.707)
        linkCPos += abs(linkAPos * 0.707)
        linkAPos = 0

    if linkBPos <= 0:
        linkAPos += abs(linkBPos * 0.707)
        linkCPos += abs(linkBPos * 0.707)
        linkBPos = 0

    if linkCPos <= 0:
        linkAPos += abs(linkCPos * 0.707)
        linkBPos += abs(linkCPos * 0.707)
        linkCPos = 0

    linkAPos = linkAPos * (maxPulse - minPulse) + minPulse
    linkBPos = linkBPos * (maxPulse - minPulse) + minPulse
    linkCPos = linkCPos * (maxPulse - minPulse) + minPulse

    linkAPos = min(max(minPulse, linkAPos), maxPulse)
    linkBPos = min(max(minPulse, linkBPos), maxPulse)
    linkCPos = min(max(minPulse, linkCPos), maxPulse)

    return int(linkAPos), int(linkBPos), int(linkCPos)


def sender():
    global sendFreq, msg, host
    while True:
        sock.sendto(json.dumps(msg, ensure_ascii=False).encode("utf8"), host)  # отправляем сообщение в виде json файла
        time.sleep(1 / sendFreq)


threading.Thread(target=sender, daemon=True).start()

glTimer = time.time()

try:
    while True:
        t = time.time() - glTimer
        control()
        time.sleep(0.005)
except KeyboardInterrupt:
    print('\nDone')
