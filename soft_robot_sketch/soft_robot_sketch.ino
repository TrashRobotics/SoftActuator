#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "ArduinoJson.h"
#include <Adafruit_PWMServoDriver.h>
#include "Wire.h"

#define PCA9685_ADRESS  0x40  
#define LED     D0           
#define SDA_PIN D1  // 
#define SCL_PIN D2  //

#define UDP_PORT 5000

#define SERVO_FREQ 50
#define SERVO_MIN_PULSE   900
#define SERVO_MAX_PULSE   2100

const char* ssid     = "Home";         // сюда вписать ssid и пароль от точки доступа
const char* password = "password";

WiFiUDP udp;                      
StaticJsonDocument<300> jsondoc;
Adafruit_PWMServoDriver pwmsd = Adafruit_PWMServoDriver(PCA9685_ADRESS);

uint32_t moveTimer = 0;
int16_t pulseList[12] = {900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900, 900};


void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);    
  Wire.begin(SDA_PIN, SCL_PIN);
  pwmsd.begin();
  pwmsd.setOscillatorFrequency(27000000);
  pwmsd.setPWMFreq(SERVO_FREQ); 
  
  for(uint8_t i = 0; i < 12; i++){
    int16_t pulse = constrain(pulseList[i], SERVO_MIN_PULSE, SERVO_MAX_PULSE);
    pwmsd.writeMicroseconds(i, pulse);
    delay(200);
  }

  WiFi.begin(ssid, password);             
  while (WiFi.status() != WL_CONNECTED) { 
    delay(1000);
  }

  udp.begin(UDP_PORT);
  delay(200);
  moveTimer = millis();
}


void loop() {
  checkUdp();
  if (millis() - moveTimer >= 20
  ){ 
    setServoPos(pulseList);
    moveTimer = millis();
  }
}


void setServoPos(int16_t* pulses){
  for(uint8_t i = 0; i < 12; i++){
    int16_t pulse = constrain(pulses[i], SERVO_MIN_PULSE, SERVO_MAX_PULSE);
    pwmsd.writeMicroseconds(i, pulse);
    //Serial.print(pulse);
    //Serial.print(", ");
  }
  //Serial.println(' ');
}


void checkUdp()
{
  static uint32_t ledTimer = 0;
  static uint8_t udpRecBuffer[255];

  if(millis()-ledTimer >= 200) digitalWrite(LED, HIGH);
  
  uint16_t packetSize = udp.parsePacket();
  if (packetSize) {
    uint16_t len = udp.read(udpRecBuffer, 255);
    if (len > 0)  udpRecBuffer[len] = '\0';
    
    DeserializationError err = deserializeJson(jsondoc, udpRecBuffer);
    if (err != DeserializationError::Ok) return;    
    copyArray(jsondoc["pulses"], pulseList);
    digitalWrite(LED, LOW);
    ledTimer = millis();
  }
}
